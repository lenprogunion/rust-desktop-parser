use std::{ fs};

#[derive(Clone, Default)]
pub struct DesktopStruct{
    pub name: String,
    pub exec: String,
    pub icon: String,
}

#[derive(Clone, Debug)]
enum Token{
    Directive(String),
    Tag{
        name: String,
        value: String,
    },
}

pub fn desktop_parser(path: String) -> Vec<DesktopStruct>{
    let paths = fs::read_dir(&path).unwrap();
    let mut execs = Vec::<DesktopStruct>::new();
    for path in paths {
        if let Ok(entry) = path{
            if let Some(extension) = entry.path().extension() {
                if extension == "desktop"{
                    let file = std::fs::read(entry.path());
                    let data_from_file = String::from_utf8(file.unwrap()).unwrap();
                    let tokens = lexer(data_from_file.clone());
                    let data_from_tokens = parse_tokens(tokens.clone());
                    execs.push(data_from_tokens);
                }
            }
        }
    }
    execs
   
}

fn parse_tokens(tokens: Vec<Token>) -> DesktopStruct{
    let mut tag_exec = String::new();
    let mut tag_name = String::new();
    let mut tag_icon = String::new();
    let mut is_directive = true;
    for token in tokens.iter(){
        match token{
            Token::Tag { name, value } => {
                if !is_directive{ continue; }
                if name.is_empty() || value.is_empty(){ continue; };
                match name.as_str(){
                    "Exec" => {
                        tag_exec = value.to_string();
                    },
                    "Name" => {
                        tag_name = value.to_string();
                    },
                    "Icon" => {
                        tag_icon = value.to_string();
                    },
                    _ => {
                        
                    }
                }
            },
            Token::Directive(name) => {
                if name != "Desktop Entry"{
                    is_directive = false;
                }
            }
        }
    }
    DesktopStruct{
        name: tag_name,
        exec: tag_exec,
        icon: tag_icon,
    }
}

fn lexer(text: String) -> Vec<Token>{
    let mut tokens = Vec::<Token>::new();
    let mut text = text.chars();
    let mut word = String::new();
    let mut is_directive = false;
    let mut tag_name = String::new();
    let mut is_tag_value = false;
    let mut is_comment = false;
    while let Some(symbol) = text.next(){
        match symbol {
            '#' => {
                is_comment = true;
            },
            '[' => {
                if is_comment{ continue; }
                if !word.is_empty(){
                    word.push(symbol);
                    continue;
                }
                is_directive = true;
            },
            ']' => {
                if is_comment{ continue; }
                if !is_directive{
                    word.push(symbol);
                    continue;
                }
                tokens.push(Token::Directive(word.clone()));
                word.clear();
                is_directive = false;
            },
            '=' => {
                if is_comment{ continue; }
                if is_tag_value{ 
                    word.push(symbol);
                    continue; 
                }
                tag_name = word.clone();
                is_tag_value = true;
                word.clear();
            },
            '\n' => {
                is_comment = false;
                tokens.push(Token::Tag{
                    name: tag_name.clone(),
                    value: word.clone(),    
                });
                tag_name.clear();
                word.clear();
                is_tag_value = false;
            },
            _ => {
                word.push(symbol);
            }
        }
    }
    tokens
}
